<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}

if ($_SESSION['role'] == 'R' || $_SESSION['role'] == 'A' || $_SESSION['role'] == 'P') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Work Order</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .wrapperb{ margin: 0 auto; width: 40%; padding: 20px; }
                .parentwrapa {margin: auto; width: 700px; clear: both;}
                .parentwrapb {width: 100%; }
                .parentwrapc {margin: auto; width: 350px;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            require_once "/home/bcmdev/include/dbconnect.php";
            require_once '/home/bcmdev/include/Exception.php';
            require_once '/home/bcmdev/include/PHPMailer.php';
            require_once '/home/bcmdev/include/SMTP.php';

            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                $ra_name = $ra_phone = $bldg_name = $program_name = $room_num = $description = "";


                $ra_name = trim($_POST['ra_name']);
                $ra_username = trim($_SESSION["username"]);
                $ra_phone = trim($_POST['ra_phone']);
                $bldg_name = trim($_POST['bldg_name']);
                $room_num = trim($_POST['room_num']);
                $subroom = trim($_POST['sub_room']);
                $description = trim($_POST['description']);
                $date = trim($_POST['date']);


                // Prepare an insert statement
                $sql = "INSERT INTO work_order (date, ra_username, ra_name, ra_phone, bldg_name, room_num, sub_room, description) "
                        . "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                if ($workQuery = mysqli_prepare($bcmdev, $sql)) {
                    mysqli_stmt_bind_param($workQuery, "ssssssss", $date, $ra_username, $ra_name, $ra_phone, $bldg_name, $room_num, $subroom, $description);
                    if (mysqli_stmt_execute($workQuery)) {
                        print("<h2>Work order submitted successfully.</h2>");
                    } else {
                        die("Something went wrong. Please try again later.");
                    }
                    mysqli_stmt_close($workQuery);
                    mysqli_close($bcmdev);

                    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = 'bespin.mcs.uvawise.edu';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = 'bcmdev';                 // SMTP username
                        $mail->Password = 'H4tcH5uck5!';                           // SMTP password
                        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 25;                                    // TCP port to connect to
                        //Recipients
                        $mail->setFrom('bcmdev@uvawise.edu', 'HDoc');
                        $mail->addAddress($_SESSION["username"] . '@uvawise.edu');     // Add a recipient
                        $mail->addReplyTo('NoReply@mcs.uvawise.edu', 'DoNotReply');

                        //Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Work Order Submitted - Do Not Reply';
                        $mail->Body = '<h2>You have successfully submitted a work order for ' . $bldg_name . ' on ' . date("l") . ', ' . date("m/d/Y") . '.</h2>';

                        $mail->send();
                        echo 'Email has been sent';
                    } catch (Exception $e) {
                        echo 'Email could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }
                } else {
                    die("Something went wrong. Please try again later.");
                }
            } else {
                ?>
                <h2>Work Order Submission</h2>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="parentwrapa">
                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Your Name:</legend>
                                <input type="text" name="ra_name"class="form-control" required="true">
                            </div>
                        </div>


                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Phone Number:</legend>
                                <input type="text" class="form-control" name="ra_phone" required="true">
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapc">
                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Building Name:</legend>
                                <select class="form-control" name="bldg_name" required="true">
                                    <option value="Asbury" >Asbury</option>
                                    <option value="Culbertson" >Culbertson</option>
                                    <option value="Commonweath" >Commonwealth</option>
                                    <option value="Martha Randolph" >Martha Randolph</option>
                                    <option value="McCraray" >McCraray</option>
                                    <option value="Thompson" >Thompson</option>
                                    <option value="House #5">House #5</option>
                                    <option value="Cav House">Cav House</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapa">
                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Room Number:</legend>
                                <input type="number" class="form-control" name="room_num" required="true">
                            </div>
                        </div>

                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Sub-Room:</legend>
                                <select class="form-control" name="sub_room" required="true">
                                    <option value="N/A">N/A</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Description of Work:</legend>
                                <textarea class="form-control" name="description" required="true"></textarea>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="date" value='<?php echo date("Y-m-d"); ?>'>

                    <div class="parentwrapa">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form><br>

                <?php
                $stmt = "SELECT * FROM work_order WHERE ra_username = '$_SESSION[username]' ORDER BY date DESC LIMIT 5";
                if ($result = $bcmdev->query($stmt)) {
                    if ($result->num_rows == 0) {
                        ?>
                        <h2>No recent submissions.</h2>
                        <?php
                    } else {
                        ?>
                        <table class="table table-striped">
                            <thead><tr><th>Date</th><th>Building</th><th>Room Number</th><th>Description</th></tr></thead>
                            <tbody>
                                <?php
                                while ($programRow = $result->fetch_assoc()) {
                                    if ($programRow["sub_room"] == "A" || $programRow["sub_room"] == "B" || $programRow["sub_room"] == "C") {
                                        $subroom = $programRow["sub_room"];
                                    } else {
                                        $subroom = "";
                                    }
                                    ?>
                                    <tr>
                                        <td class="align-middle"><?php print($programRow["date"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["bldg_name"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["room_num"] . $subroom); ?></td>
                                        <td class="align-middle"><?php print($programRow["description"]); ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                } else {
                    die('Database error. [' . $bcmdev->error . ']');
                }
                $bcmdev->close();
            }
            if ($_SESSION['role'] == 'R') {
                include_once 'footer.php';
            }
            ?>
        </body>
    </html>
    <?php
} else {
    header("location: index.php");
}
    