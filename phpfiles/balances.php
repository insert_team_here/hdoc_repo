<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}

//determine role of logged in individual, redirect if role is RA or Maintenance
if ($_SESSION['role'] == 'R' || $_SESSION['role'] == 'M') {
    header("location: index.php"); //redirect to landing page
} else if ($_SESSION['role'] == 'A' || $_SESSION['role'] == 'P') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Balances</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style>
                body { background-color: lightblue;}
            </style>
        </head>
        <body>
            <?php
            include_once 'navbar.php';
            require_once '/home/bcmdev/include/dbconnect.php';
            
            //determine server request method and execute balance update when necessary
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (isset($_POST['tempAmt']) && $_POST['tempAmt'] >= 0 && is_numeric($_POST['tempAmt'])) {
                    $newAmt = $_POST['tempAmt']; //assign variables
                    $updId = $_POST['tempId']; //assign variables
                    $stmt = "UPDATE user SET balance = ? WHERE id = ?";
                    if ($updBalQuery = mysqli_prepare($bcmdev, $stmt)) { //prepare sql statement
                        mysqli_stmt_bind_param($updBalQuery, "ss", $newAmt, $updId); //bind variables to query
                        if (mysqli_stmt_execute($updBalQuery)) { //execute sql statmement
                            print("<h2>Balance updated.</h2>");
                        } else {
                            die("Something went wrong. Please try again later."); //show error
                        }
                        mysqli_stmt_close($updBalQuery);
                    } else {
                        die("Something went wrong. Please try again later."); //show error
                    }
                } else {
                    print("<h2>You must enter a positive number for the dollar amount.</h2>"); //show error
                }
            }

            //shows a list of users with RA role
            $stmt = "SELECT id, firstname, lastname, username, balance FROM user WHERE role = 'R' ORDER BY lastname";
            if ($result = $bcmdev->query($stmt)) {
                ?>
                <table class="table table-striped">
                    <thead><tr><th>Username</th><th>First Name</th><th>Last Name</th><th>Balance</th><th>Enter new balance:</th></tr></thead>
                    <tbody>
                        <?php
                        while ($userRow = $result->fetch_assoc()) { //print results in table with modification fields
                            ?>
                            <tr>
                                <td class="align-middle"><?php print($userRow["username"]); ?></td>
                                <td class="align-middle"><?php print($userRow["firstname"]); ?></td>
                                <td class="align-middle"><?php print($userRow["lastname"]); ?></td>
                                <td class="align-middle"><?php print("$" . $userRow["balance"]); ?></td>
                                <!--The form embedded in this table uses inputs and a submit.-->
                                <td class="align-middle"><form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method='POST'>
                                        <input type='hidden' name='tempId' value='<?php echo $userRow["id"] ?>' />
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="number" class="form-control" name="tempAmt">
                                            <div style="padding-left: 5px"><input type='submit' class='btn btn-primary' value='Update' /></div>
                                        </div></form></td>
                            </tr>
                            <?php
                        }
                    } else {
                        die('Database error. [' . $bcmdev->error . ']'); //show error
                    }
                    $bcmdev->close(); //close connection
                    ?>
                </tbody>
            </table>
        </body>
    </html>
    <?php
} else {
    header("location: index.php"); //redirect to landing page
}

