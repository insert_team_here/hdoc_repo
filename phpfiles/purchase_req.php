<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}

if ($_SESSION['role'] == 'R') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Purchase Request Submission</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">

            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .wrapperb{ width: 40%; padding: 20px; float: left;}
                .parentwrapa {margin: auto; width: 700px;}
                .parentwrapb {margin-left: 28%; width: 100%; clear: both;}
                textarea {width: 90%; height: 200px;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            ?>

            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                require_once '/home/bcmdev/include/Exception.php';
                require_once '/home/bcmdev/include/PHPMailer.php';
                require_once '/home/bcmdev/include/SMTP.php';

                $raName = $contact = $email = $progName = $progDate = $progTime = $progLoc = $progStatus = $numPeople = "";
                $pizzaLoc = $pizzaNum = $pizzaTime = $otherSupplies = $webLink = $subName = $subDate = $subTime = "";

                $raName = trim($_POST['raname']);
                $contact = trim($_POST['contactnum']);
                $email = trim($_POST['email']);
                $progName = trim($_POST['progname']);
                $progDate = trim($_POST['progdate']);
                $progTime = trim($_POST['progtime']);
                $progLoc = trim($_POST['progloc']);
                $progStatus = trim($_POST['progstatus']);
                $numPeople = trim($_POST['numpeople']);
                $pizzaLoc = trim($_POST['pizzaloc']);
                $pizzaNum = trim($_POST['pizzanum']);
                $pizzatime = trim($_POST['pizzatime']);
                $otherSupplies = trim($_POST['othersupplies']);
                $webLink = trim($_POST['weblink']);
                $subName = trim($_SESSION['firstname'] . " " . $_SESSION['lastname']);
                $subDate = trim(date("Y-m-d"));
                $subTime = trim(date("h:ia"));
                ?>

                <h2>Purchase request submitted.</h2>

                <?php
                $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = 'bespin.mcs.uvawise.edu';  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = 'bcmdev';                 // SMTP username
                    $mail->Password = 'H4tcH5uck5!';                           // SMTP password
                    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 25;                                    // TCP port to connect to
                    //Recipients
                    $mail->setFrom('bcmdev@uvawise.edu', 'HDoc');
                    $mail->addAddress($_SESSION["username"] . '@uvawise.edu');     // Add a recipient
                    $mail->addReplyTo('NoReply@mcs.uvawise.edu', 'DoNotReply');

                    //Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Purchase Request Submitted - Do Not Reply';
                    $mail->Body = '<h2>You have successfully submitted a Purchase Request for ' . $progName . ' occuring on ' . $progDate . '.</h2>';

                    $mail->send();
                    echo 'Email has been sent';
                } catch (Exception $e) {
                    echo 'Email could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
            } else {
                ?>
                <h1>Purchase Request</h1>
                <!--may not need the post method-->
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="parentwrapa">
                        <div class="wrappera">
                            <div class="form-group">
                                <h2>RA Info</h2>
                                <label>RA Name:</label>
                                <input type="text" name="raname"class="form-control" required="true"><br>

                                <label>Preferred Contact Number:</label>
                                <input type="text" name="contactnum"class="form-control" required="true"><br>

                                <label>Email Address:</label>
                                <input type="text" name="email"class="form-control" value="<?php echo $_SESSION["username"]; ?>@uvawise.edu" required="true"><br>
                            </div>
                        </div>

                        <div class="wrappera">
                            <div class="form-group">
                                <h2>Program Info</h2>
                                <label>Program Name:</label>
                                <input type="text" name="progname"class="form-control" required="true"><br>

                                <label>Date of Program:</label>
                                <input type="date" name="progdate"class="form-control" required="true"><br>

                                <label>Time of Program:</label>
                                <input type="text" name="progtime"class="form-control" required="true"><br>

                                <label>Location of Program:</label>
                                <input type="text" name="progloc"class="form-control" required="true"><br>

                                <label>Has this program been approved by your ADRL:</label>
                                <select class="form-control" name="progstatus" required="true">
                                    <option value="Yes" >Yes</option>
                                    <option value="No" >No</option>
                                </select><br>

                                <label>How many people do you expect at your program:</label>
                                <input type="text" name="numpeople"class="form-control" required="true"><br>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <h2>Request Details</h2><!--start next time by changing some of the legends to h1's-->
                                <legend>Pizza Purchase</legend>
                                <p>(Only complete this section only if you are requesting a pizza purchase.)</p>
                                <label>Where is the pizza being delivered to:</label>
                                <input type="text" name="pizzaloc"class="form-control"><br>

                                <p>Number and types of pizza to be delivered. Please be specific:<br><br>
                                    <textarea name="pizzanum" class="form-control"></textarea></p>

                                <label>Delivery Time:</label>
                                <input type="text" name="pizzatime" class="form-control"><br>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Other Program Supplies</legend><br>
                                <p>(Only complete this section if you are requiring other supplies for your program.)</p>
                                <p>Supplies Needed:<br><br>
                                    <textarea name="othersupplies" class="form-control"></textarea></p>

                                <p>If you found this program online, please list the web link here:<br><br>
                                    <input type="text" name="weblink" class="form-control"><br>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div><br>
                </form>
                <?php
            }
            include_once 'footer.php';
            ?>
        </body>
    </html>
    <?php
} else {
    header("location: index.php");
}