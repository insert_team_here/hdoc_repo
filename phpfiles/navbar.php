<?php
if ($_SESSION['role'] == 'R') {
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <nav class="topnav">
                <a href="index.php">Home</a>
                <a href="program.php">Program Proposal</a>
                <a href="purchase_req.php">Purchase Request</a>
                <a href="workorder.php">Work Order</a>
                <a href="dutylog.php">Duty Log</a>
                <a href="logout.php" style="background-color:red; float:right">Log Out</a>
            </nav>
        </body>
    </html>
    <?php
} else if ($_SESSION['role'] == 'A' || ($_SESSION['role'] == 'P')) {
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <nav class="topnav">
                <a href="index.php">Home</a>
                <a href="program.php">Program Proposal</a>
                <a href="historical_programs.php">Historical Program Search</a>
                <a href="workorder.php">Work Order</a>
                <a href="workorderhistory.php">Historical Work Orders</a>
                <a href="dutylog.php">Duty Log</a>
                <a href="balances.php">Balances</a>
                <a href="userlist.php">User Maintenance</a>
                <a href="logout.php" style="background-color:red; float:right">Log Out</a>
            </nav>
        </body>
    </html>
    <?php
} else {
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <nav class="topnav">
                <a href="index.php">Home</a>
                <a href="workorderhistory.php">Historical Work Orders</a>
                <a href="logout.php" style="background-color:red; float:right">Log Out</a>
            </nav>
        </body>
    </html>
    <?php
}

