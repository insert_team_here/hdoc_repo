<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}

// if role is resident advisor, this page will generate
if ($_SESSION['role'] == 'R') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Duty Log Submission</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">

            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .wrapperb{ width: 40%; padding: 20px; float: left; }
                .parentwrapa {margin: auto; width: 700px;}
                .parentwrapb {margin-left: 10%; width: 100%; clear: both; }
                textarea {width: 90%; height: 200px;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';

            // if POST, process data
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                require_once '/home/bcmdev/include/dbconnect.php';
                require_once '/home/bcmdev/include/Exception.php';
                require_once '/home/bcmdev/include/PHPMailer.php';
                require_once '/home/bcmdev/include/SMTP.php';

                //assignment of variables
                $firstname = $lastname = $email = $stime = $etime = $interactions = "";
                $maintenance = $sanitation = "";
                $damages = $date = "";

                $firstname = trim($_POST['firstname']);
                $lastname = trim($_POST['lastname']);
                $email = trim($_POST['email']);
                $stime = trim($_POST['StartTime']);
                $etime = trim($_POST['EndTime']);
                $date = trim($_POST['date']);
                $interactions = trim($_POST['InteractionsInput']);
                $maintenance = trim($_POST['MaintenanceAndHousekeepingInput']);
                $sanitation = trim($_POST['SecurityAndSanitationInput']);
                $damages = trim($_POST['DamagesAndOtherInput']);

                // Prepare an insert statement
                $sql = "INSERT INTO duty_log (fname, lname, email, date, stime, etime, interactions, maintenance, sanitation, damages) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                if ($insertDLQuery = mysqli_prepare($bcmdev, $sql)) {
                    mysqli_stmt_bind_param($insertDLQuery, "ssssssssss", $firstname, $lastname, $email, $stime, $etime, $date, $interactions, $maintenance, $sanitation, $damages);
                    if (mysqli_stmt_execute($insertDLQuery)) { //execute statement
                        print("<h2>Duty log submitted successfully.</h2>");
                    } else {
                        die("Something went wrong. Please try again later.");
                    }
                    mysqli_stmt_close($insertDLQuery); //closing query
                    mysqli_close($bcmdev); //closing connection

                    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = 'bespin.mcs.uvawise.edu';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = 'bcmdev';                 // SMTP username
                        $mail->Password = 'H4tcH5uck5!';                           // SMTP password
                        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 25;                                    // TCP port to connect to
                        //Recipients
                        $mail->setFrom('bcmdev@uvawise.edu', 'HDoc');
                        $mail->addAddress($_SESSION["username"] . '@uvawise.edu');     // Add a recipient
                        $mail->addReplyTo('NoReply@mcs.uvawise.edu', 'DoNotReply');

                        //Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Duty Log Submitted - Do Not Reply';
                        $mail->Body = '<h2>You have successfully submitted a Duty Log for ' . $date . ' at ' . $stime . '.</h2>';

                        $mail->send();
                        echo 'Email has been sent';
                    } catch (Exception $e) {
                        echo 'Email could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }
                } else {
                    die("Something went wrong. Please try again later.");
                }
            } else {
                ?>

                <!--name and email input-->
                <h2>Duty Log Submission</h2>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="parentwrapa">
                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Your Info:</legend>
                                <label>First Name:<sup>*</sup></label>
                                <input type="text" name="firstname" class="form-control" required="true"><br>

                                <label>Last Name:<sup>*</sup></label>
                                <input type="text" name="lastname" class="form-control" required="true"><br>

                                <label>Email:<sup>*</sup></label>
                                <input type="email" name="email" class="form-control" value="<?php echo $_SESSION["username"]; ?>@uvawise.edu" required="true">
                            </div>
                        </div>
                        <!--date and times-->

                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Date & Time:</legend>
                                <label>Start Time:*</label>
                                <input type="text" name="StartTime" class="form-control" required="true"><br>
                                <label>End Time:*</label>
                                <input type="text" name="EndTime" class="form-control" required="true">
                            </div>
                            <div class="form-group">
                                <label>Date:</label>
                                <input type="date" name="date" class="form-control" required="true">
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>

                    <!--interactions and maintenance-->
                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Interactions:</legend>
                                Please include names when possible.
                                <textarea name="InteractionsInput" class="form-control"></textarea></p>
                            </div>
                        </div>

                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Maintenance & Housekeeping:</legend>
                                List any maintenance issues.
                                <textarea name="MaintenanceAndHousekeepingInput" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <!--security and damages and other-->
                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Security and Sanitation:</legend>
                                List any security or sanitation issues.
                                <textarea name="SecurityAndSanitationInput" class="form-control"></textarea>
                            </div>
                        </div>


                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Damages & Other Issues:</legend>
                                List any damages or other issues.
                                <textarea name="DamagesAndOtherInput" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <!--submit button-->
                    <div class="parentwrapa">
                        <div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>

                <?php
                include_once 'footer.php';
                ?>
            </body>
        </html>

        <?php
    }
    // if role is admin or professional staff, generate this page
} else if ($_SESSION['role'] == 'A' || $_SESSION['role'] == 'P') {
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Duty Log Retrieval</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .wrapperb{ width: 40%; padding: 20px; float: left; border-radius: 10px; background-color: lightblue;}
                .parentwrapa {margin: auto; width: 700px;}
                .parentwrapb {margin-left: 10%; width: 100%; clear: both;}
                textarea {width: 90%; height: 200px;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            ?>

            <!--date form to search with-->
            <h2>Duty Log Retrieval</h2>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <div class="parentwrapa">
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>From:</legend>
                            <input type="date" name="StartDate" required="true" class="form-control"></p>
                        </div>
                    </div>
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>To:</legend>
                            <input type="date" name="EndDate" required="true" class="form-control"></p>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>

                <div class="parentwrapa">
                    <div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form><br><br>

            <?php
            //if POST, process data and show results
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                require_once '/home/bcmdev/include/dbconnect.php';

                #when the button is pressed, get the start and end dates
                $qSDate = trim($_POST['StartDate']);
                $qEDate = trim($_POST['EndDate']);
                #prepare the statement
                $stmt = "SELECT fname, lname, email, date, stime, etime, interactions, maintenance, sanitation, damages FROM duty_log WHERE date >= '$qSDate' AND date <= '$qEDate'";
                #if result isn't empty, create a table with the data
                if ($result = $bcmdev->query($stmt)) {
                    if ($result->num_rows == 0) { //if not results show no results
                        ?>
                        <h2>No results found.</h2>
                        <?php
                        //otherwise
                    } else {
                        ?>
                        <table class="table table-striped">
                            <thead><tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Date</th><th>Start Time</th><th>End Time</th><th>Interactions</th><th>Maintenance</th><th>Sanitation</th><th>Damages</th></tr></thead>
                            <tbody>
                                <?php
                                while ($userRow = $result->fetch_assoc()) { //generate table with results
                                    ?>
                                    <tr>
                                        <td class="align-middle"><?php print($userRow["fname"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["lname"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["email"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["date"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["stime"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["etime"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["interactions"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["maintenance"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["sanitation"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["damages"]); ?></td>
                                    </tr>

                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                } else {
                    die('Database error. [' . $bcmdev->error . ']'); //display error
                }
                $bcmdev->close(); //close connection
            }
            ?>
        </body>
    </html>
    <?php
} else {
    header("location: index.php"); //redirect to landing page
}
    