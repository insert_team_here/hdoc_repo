<?php
/*
 * Most of the code in this file was obtained from 
 * https://www.tutorialrepublic.com/php-tutorial/php-mysql-login-system.php
 * 
 * Some modifications have been made to suit our needs.
 * 
 */
// Initialize the session
session_start();

// Unset all of the session variables
$_SESSION = array();

// Destroy the session.
session_destroy();

// Redirect to login page
header("location: login.php");
exit;
