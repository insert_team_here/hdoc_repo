<?php
if ($_SESSION['role'] == 'R') {
    $role = 'Resident Advisor';
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <div class="footer">
                Hi, <?php print $_SESSION["firstname"]; ?>! Your current programming balance is: <strong>$<?php print $_SESSION['balance'] ?></strong>
            </div>
        </body>
    </html>
    <?php
} else if ($_SESSION['role'] == 'P') {
    $role = 'Professional Staff';
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <div class="footer">
                Your current role is <?php print $role ?>
            </div>
        </body>
    </html>
    <?php
} else if ($_SESSION['role'] == 'M') {
    $role = 'Maintenance';
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <div class="footer">
                Your current role is <?php print $role ?>
            </div>
        </body>
    </html>
    <?php
} else if ($_SESSION['role'] == 'A') {
    $role = 'Administrator';
    ?>

    <!DOCTYPE html>
    <html>
        <body>
            <div class="footer">
                Your current role is <?php print $role ?>
            </div>
        </body>
    </html>
    <?php
}
