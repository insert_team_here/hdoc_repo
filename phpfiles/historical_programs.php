<?php
// Include config file
require_once '/home/bcmdev/include/dbconnect.php';

// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}
if ($_SESSION['role'] == 'P' || $_SESSION['role'] == 'A') {#if you're an admin or prostaff
    ?>
    <!--copied this stuff from another page. Made an adjustment or two to get the 3 inputs on one line.-->
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Historical Program Proposals</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 225px; padding: 20px; float: left;}
                .wrapperb{ width: 40%; padding: 20px; float: left; background-color: lightgrey;}
                .parentwrapa {margin: auto; width: 700px;}
                .parentwrapb {margin-left: 10%; width: 100%; clear: both;}
                textarea {width: 90%; height: 200px;}
                .wrapper{ margin: 0 auto; width: 350px; padding: 20px; }
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            ?>

            <h2>Historical Program Proposals</h2>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <div class="parentwrapa">
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>Start Date:</legend>
                            <input type="date" name="startDate" class="form-control"></p>
                        </div>
                    </div>
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>End Date:</legend>
                            <input type="date" name="endDate" class="form-control"></p>
                        </div>
                    </div>
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>Username:</legend>
                            <input type="text" name="Username" class="form-control"></p>
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div> 
                        <button type="submit" class="btn btn-primary" style="display: block; margin: 0 auto;">Search</button>
                    </div>
                </div>
            </form>

            <?php
            $execute = false;
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                #gets info and creates stmt accordingly
                if (!empty(trim($_POST['startDate'])) AND ! empty(trim($_POST['endDate'])) and empty(trim($_POST['Username']))) {
                    $sdate = trim($_POST['startDate']);
                    $edate = trim($_POST['endDate']);
                    $stmt = "SELECT * FROM program_proposal "
                            . "WHERE program_status = 'A' AND program_date >= '$sdate' AND program_date <= '$edate' "
                            . "OR program_status = 'D' AND program_date >= '$sdate' AND program_date <= '$edate'";
                    $execute = true;
                } else if (empty(trim($_POST['startDate'])) AND empty(trim($_POST['endDate'])) and ! empty(trim($_POST['Username']))) {
                    $uname = trim($_POST['Username']);
                    $stmt = "SELECT * FROM program_proposal "
                            . "WHERE program_status = 'A' AND ra_username = '$uname' OR program_status = 'D' AND ra_username = '$uname'";
                    $execute = true;
                } else if (!empty(trim($_POST['startDate'])) AND ! empty(trim($_POST['endDate'])) and ! empty(trim($_POST['Username']))) {
                    $sdate = trim($_POST['startDate']);
                    $edate = trim($_POST['endDate']);
                    $uname = trim($_POST['Username']);
                    $stmt = "SELECT * FROM program_proposal "
                            . "WHERE program_status='A' AND program_date >= '$sdate' AND program_date <= '$edate' AND ra_username = '$uname' OR program_status='D' AND program_date >= '$sdate' AND program_date <= '$edate' AND ra_username = '$uname'";
                    $execute = true;
                } else {
                    ?>
                    <h2>Search parameters entered incorrectly.</h2>
                    <?php
                }
            }
            if ($execute) {
                if ($result = $bcmdev->query($stmt)) {
                    if ($result->num_rows == 0) {
                        ?>
                        <h2>No results found.</h2>
                        <?php
                    } else {
                        ?>
                        <table class="table table-striped">
                            <thead><tr><th>Name</th><th>Username</th><th>Program Name</th><th>Location</th><th>Date</th><th>Time</th><th>Category</th><th>Description</th>
                                    <th>Funds</th><th>Items</th><th>Faculty</th><th>Status</th><th>Feedback</th><th>Date Created</th></tr></thead>
                            <tbody>
                                <?php
                                while ($userRow = $result->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td class="align-middle"><?php print($userRow["ra_name"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["ra_username"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_name"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_location"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_date"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_time"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_category"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_description"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_funds"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_items"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_faculty"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_status"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["program_feedback"]); ?></td>
                                        <td class="align-middle"><?php print($userRow["creation_date"]); ?></td>
                                        <?php
                                    }
                                    ?>
                            </tbody>
                        </table>
                        <?php
                    }
                } else {
                    die('Database error. [' . $bcmdev->error . ']');
                }
                $bcmdev->close();
            }
            ?>
        </body>
    </html>
    <?php
} else {#if you aren't admin or prostaff
    header("location: index.php");
}


    