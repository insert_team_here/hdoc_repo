<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}

//determining role from session variable
if ($_SESSION['role'] == 'R' || $_SESSION['role'] == 'M') {
    header("location: index.php"); //if role is ra or maint, redirect to landing page
} else if ($_SESSION['role'] == 'A' || $_SESSION['role'] == 'P') { //othewise
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>User Maintenance</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style>
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .parentwrapa {margin: auto; width: 400px;}

            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            require_once '/home/bcmdev/include/dbconnect.php';


            //if server method is post
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                //if role button was clicked, generate form to change role
                if (isset($_POST['Role'])) {
                    ?>
                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method='POST'>
                        <!--drop down menu for new role-->
                        <div class="parentwrapa">
                            <div class="wrappera">
                                <h3>Select new role:</h3>
                                <select class="form-control" name="new_role" required="true">
                                    <option value="M" >Maintenance</option>
                                    <option value="R" >Resident Advisor</option>
                                    <option value="P" >Professional Staff</option>
                                    <option value="A" >Administrator</option>
                                </select><br>
                                <input type='hidden' name="tempId" value="<?php echo trim($_POST["tempId"]); ?>"/> <!--passing through tuple id once more-->
                                <input type='submit' class='btn btn-primary' name="updRole" value='Update' content='Update'/>
                                <input type='submit' class='btn btn-primary' name="cancel" value='Cancel' content='Cancel'/>
                            </div>
                        </div>
                    </form>					
                    <?php
                    //if role change was submitted, update the role in the database
                } else if (isset($_POST['updRole'])) {
                    //assignment of variables
                    $tempId = trim($_POST["tempId"]);
                    $new_role = trim($_POST["new_role"]);
                    $sql = "UPDATE user SET role = '$new_role'"
                            . " WHERE id = '$tempId'";
                    if ($result = $bcmdev->query($sql)) { //if role is success, display so
                        ?>
                        <h2>Role Updated!</h2>
                        <?php
                    } else {
                        die("Query failed."); //show error if error
                    }
                    //if password button is clicked, show form for new password
                } else if (isset($_POST['Password'])) {
                    ?>
                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method='POST'>
                        <!--input fields for new password-->
                        <div class="parentwrapa">
                            <div class="wrappera">
                                <h3>Enter new password:</h3>
                                <div class = "form-group">
                                    <label>Password:</label>
                                    <input type = "password" name = "password" class = "form-control">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password:</label>
                                    <input type="password" name="confirm_password" class="form-control">
                                </div>
                                <input type='hidden' name="tempId" value='<?php echo trim($_POST["tempId"]); ?>'/> <!--passing tuple id once more-->
                                <input type='submit' class='btn btn-primary' name="confPass" value='Update' content='Update'/>
                                <input type='submit' class='btn btn-primary' name="cancel" value='Cancel' content='Cancel'/>
                            </div>
                        </div>
                    </form>
                    <?php
                    //if submitting password change, sanitize it, insert it, and show a confirmation
                } else if (isset($_POST['confPass'])) {
                    //assignment of variabls
                    $tempId = trim($_POST["tempId"]);
                    $pass = trim($_POST["password"]);
                    $confPass = trim($_POST["confirm_password"]);
                    //confirm passwords match
                    if ($pass == $confPass && strlen($pass) >= 6 && strlen($confPass) >= 6) {
                        //hash and salt password
                        $hashed_pass = password_hash($pass, PASSWORD_DEFAULT);
                        $sql = "UPDATE user SET password = ?"
                                . " WHERE id = ?";
                        if ($updPassQuery = mysqli_prepare($bcmdev, $sql)) { //prepare statement
                            mysqli_stmt_bind_param($updPassQuery, "ss", $hashed_pass, $tempId); //bind parameters
                            if (mysqli_stmt_execute($updPassQuery)) { //execute query
                                print("<h2>Password updated.</h2>"); //show success
                            } else {
                                die("Something went wrong. Please try again later."); //otherwise, show error
                            }
                            mysqli_stmt_close($updPassQuery); //close the statement
                        } else {
                            die("Something went wrong. Please try again later."); //show error
                        }
                        //if passwords don't match, say so
                    } else {
                        ?>
                        <h2>Passwords do not match, or do not meet the length requirement.</h2>
                        <?php
                    }
                    //if delete button is clicked show a form for deletion
                } else if (isset($_POST['Delete'])) {
                    ?>
                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method='POST'>
                        <!--show warning of deletion-->
                        <h3>Are you sure you want to delete this user?</h3>
                        <input type='hidden' name="tempId" value='<?php echo trim($_POST["tempId"]); ?>'/> <!-- passing tuple id again-->
                        <input type='submit' class='btn btn-primary' name="confDel" value='Delete' content='Delete'/>
                        <input type='submit' class='btn btn-primary' name="cancel" value='Cancel' content='Cancel'/>
                    </form>
                    <?php
                    // if confirming delete, delete the user from the table
                } else if (isset($_POST['confDel'])) {
                    //assign variables
                    $tempId = trim($_POST["tempId"]);
                    $sql = "DELETE FROM user"
                            . " WHERE id = '$tempId'";
                    if ($result = $bcmdev->query($sql)) { //execute query
                        ?>
                        <h2>User Deleted!</h2> <!-- show a confirmation-->
                        <?php
                    } else {
                        die("Query failed."); //otherwise show error
                    }
                    /*
                     * Consider changing function to mark user inactive with 
                     * an attribute in the table in the future
                     */
                    //if cancel button is clicked anywhere, show it
                } else if (isset($_POST['cancel'])) {
                    ?>
                    <h2>Action Canceled.</h2>
                    <?php
                    //otherwise, if no checked post variables are not set, show invalid action
                } else {
                    ?>
                    <h2>Invalid Action.</h2>
                    <?php
                }
                mysqli_close($bcmdev); //close the connection
            } else {

                $stmt = "SELECT id, firstname, lastname, username, role FROM user ORDER BY lastname";
                if ($result = $bcmdev->query($stmt)) { //execute statement
                    ?>
                    <!--begin table-->
                    <table class="table table-striped">
                        <thead><tr><th>Username</th><th>First Name</th><th>Last Name</th><th>Role</th><th colspan="3">Change or Delete:</th></tr></thead>
                        <tbody>
                            <?php
                            while ($userRow = $result->fetch_assoc()) { //generate table with data from query
                                ?>
                            <!-- embed a form into the table with buttons for each action-->
                            <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method='POST'>
                                <tr>
                                    <td class="align-middle"><?php print($userRow["username"]); ?></td>
                                    <td class="align-middle"><?php print($userRow["firstname"]); ?></td>
                                    <td class="align-middle"><?php print($userRow["lastname"]); ?></td>
                                    <td class="align-middle"><?php print($userRow["role"]); ?></td>
                                    <!-- buttons in table cells-->
                                    <td class="align-middle">
                                        <input type='hidden' name='tempId' value='<?php echo $userRow["id"] ?>' > <!--include tuple id as a hidden input-->
                                        <input type='submit' class='btn btn-primary' name="Password" value='Password' >
                                    </td>
                                    <td><input type='submit' class='btn btn-primary' name="Role" value='Role' ></td>
                                    <td><input type='submit' class='btn btn-primary' name="Delete" value='Delete' ></td>
                                </tr>
                            </form>
                            <?php
                        }
                    } else {
                        die('Database error. [' . $bcmdev->error . ']'); //show error
                    }
                    $bcmdev->close(); //close connection
                    ?>
                </tbody>
            </table>
            <?php
        }
        ?>
    </body>
    </html>
    <?php
} else { 
    header("location: logout.php"); //redirect to index page
}

