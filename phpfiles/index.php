<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="stylesheet.css" rel="stylesheet" type="text/css">
        <style>
            body{ font: 14px sans-serif; background-color: lightblue;}
            .wrappera{ width: 350px; padding: 20px; float: left;}
            .wrapperb{ width: 40%; padding: 20px; float: left;}
            .parentwrapa {margin: auto; width: 700px;}
            .parentwrapb {margin-left: 10%; width: 100%; clear: both;}
        </style>
    </head>
    <body>

        <?php
        include_once 'navbar.php';
        ?>

        <div class="content">
            <h1>Hi, <b><?php echo $_SESSION['firstname']; ?></b>.</h1><br>
            <h2>Here are a few tips to keep in mind when using this system.</h2>
        </div>

        <ul>
            <li>Some input elements, particularly date pickers, are only compatible with current versions of Safari, Firefox, and Chrome.</li>
            <li>All dates <b>should</b> be in YYYY-MM-DD format.</li>
            <li>When submitting some forms, such as work orders, duty logs, and program proposals/purchase requests, you will see a long output of mumbo jumbo.
                <br>This is normal and expected, as it helps developers identify any issues in the email system.</li>
        </ul>

        <?php
        if ($_SESSION['role'] == 'R') {
            include_once 'footer.php';
        }
        ?>

    </body>
</html>