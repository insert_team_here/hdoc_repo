<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}
if ($_SESSION['role'] == 'R') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Program Proposal Submission</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .wrapperb{ width: 40%; padding: 20px; float: left;}
                .parentwrapa {margin: auto; width: 700px;}
                .parentwrapb {margin-left: 10%; width: 100%; clear: both;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            require_once "/home/bcmdev/include/dbconnect.php";
            ?>

            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                require_once '/home/bcmdev/include/Exception.php';
                require_once '/home/bcmdev/include/PHPMailer.php';
                require_once '/home/bcmdev/include/SMTP.php';

                $ra_name = $ra_username = $program_date = $program_name = $program_location = $program_category = $program_time = "";
                $program_description = $program_funds = $program_items = $program_faculty = "";


                $ra_name = ($_POST['ra_name']);
                $ra_username = trim($_SESSION['username']);
                $program_name = trim($_POST['program_name']);
                $program_location = trim($_POST['program_location']);
                $program_date = trim($_POST['program_date']);
                $program_time = trim($_POST['program_time']);
                $program_category = trim($_POST['program_category']);
                $program_description = trim($_POST['program_description']);
                $program_funds = trim($_POST['program_funds']);
                $program_items = trim($_POST['program_items']);
                $program_faculty = trim($_POST['program_faculty']);

                // Prepare an insert statement
                $sql = "INSERT INTO program_proposal (ra_name, ra_username, program_name, program_location, program_date, program_time, program_category, program_description, program_funds, program_items, program_faculty) "
                        . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                if ($programQuery = mysqli_prepare($bcmdev, $sql)) {
                    mysqli_stmt_bind_param($programQuery, "sssssssssss", $ra_name, $ra_username, $program_name, $program_location, $program_date, $program_time, $program_category, $program_description, $program_funds, $program_items, $program_faculty);
                    if (mysqli_stmt_execute($programQuery)) {
                        print("<h2>Program proposal submitted successfully.</h2>");
                    } else {
                        die("Something went wrong. Please try again later.");
                    }
                    mysqli_stmt_close($programQuery);

                    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = 'bespin.mcs.uvawise.edu';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = 'bcmdev';                 // SMTP username
                        $mail->Password = 'H4tcH5uck5!';                           // SMTP password
                        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 25;                                    // TCP port to connect to
                        //Recipients
                        $mail->setFrom('bcmdev@uvawise.edu', 'HDoc');
                        $mail->addAddress($_SESSION["username"] . '@uvawise.edu');     // Add a recipient
                        $mail->addReplyTo('NoReply@mcs.uvawise.edu', 'DoNotReply');
                        //Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Program Proposal Submitted - Do Not Reply';
                        $mail->Body = '<h2>You have successfully submitted a Program Proposal for ' . $program_name . ' occuring on ' . $program_date . '.</h2>';

                        $mail->send();
                        echo 'Email has been sent';
                    } catch (Exception $e) {
                        echo 'Email could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }
                } else {
                    die("Prepare went wrong. Please try again later.");
                }
            } else {
                ?>

                <h2>Program Proposal Submission</h2>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="parentwrapa">
                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Your Name:</legend>
                                <input type="text" name="ra_name"class="form-control" required="true"><br>

                            </div>
                        </div>
                        <div class="wrappera">
                            <div class="form-group">
                                <legend>Program Category:</legend>
                                <select class="form-control" name="program_category" required="true">
                                    <option value="Educational" >Educational</option>
                                    <option value="Social" >Social</option>
                                    <option value="Floor Meeting" >Floor Meeting</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Date:</legend>
                                <p><input type="date" class="form-control" name="program_date" required="true"></p>
                            </div>
                        </div>

                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Name:</legend>
                                <p><input type="text" class="form-control" name="program_name" required="true"></p>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Time:</legend>
                                <p><input type="text" class="form-control" name="program_time" required="true"></p>
                            </div>
                        </div>

                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Location:</legend>
                                <p><input type="text" class="form-control" name="program_location" required="true"></p>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Funds Required:</legend>
                                <p><input type="number" class="form-control" name="program_funds"></p>
                            </div>
                        </div>

                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Description:</legend>
                                <p><textarea class="form-control" name="program_description" required="true"></textarea></p>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapb">
                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>Program Items Needed:</legend>
                                <p><textarea class="form-control" name="program_items"></textarea></p>
                            </div>
                        </div>

                        <div class="wrapperb">
                            <div class="form-group">
                                <legend>List an attending Faculty if any:</legend>
                                <textarea class="form-control" name="program_faculty"></textarea></p>
                            </div>
                        </div>
                    </div>

                    <div class="parentwrapa">
                        <div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <br>
                </form>
                <?php
                $stmt = "SELECT * FROM program_proposal WHERE ra_username = '$_SESSION[username]' ORDER BY creation_date DESC LIMIT 10";
                if ($result = $bcmdev->query($stmt)) {
                    if ($result->num_rows == 0) {
                        ?>
                        <h2>No recent submissions.</h2>
                        <?php
                    } else {
                        ?>
                        <table class="table table-striped">
                            <thead><tr><th>Name</th><th>Location</th><th>Date</th><th>Time</th>
                                    <th>Category</th><th>Description</th><th>Funds</th><th>Items</th><th>Faculty</th>
                                    <th>Status</th><th>Feedback</th></tr></thead>
                            <tbody>
                                <?php
                                while ($programRow = $result->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td class="align-middle"><?php print($programRow["program_name"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_location"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_date"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_time"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_category"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_description"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_funds"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_items"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_faculty"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_status"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["program_feedback"]); ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                } else {
                    die('Database error. [' . $bcmdev->error . ']');
                }
                $bcmdev->close();
            }
            include_once 'footer.php';
            ?>
        </body>
    </html>
    <?php
} else if ($_SESSION['role'] == 'A' || $_SESSION['role'] == 'P') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Program Proposal</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            require_once "/home/bcmdev/include/dbconnect.php";
            ?>

            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                require_once '/home/bcmdev/include/Exception.php';
                require_once '/home/bcmdev/include/PHPMailer.php';
                require_once '/home/bcmdev/include/SMTP.php';

                $program_feedback = $tempID = "";
                $program_feedback = trim($_POST['program_feedback']);
                $tempID = trim($_POST['tempId']);

                $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = 'bespin.mcs.uvawise.edu';  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = 'bcmdev';                 // SMTP username
                    $mail->Password = 'H4tcH5uck5!';                           // SMTP password
                    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 25;                                    // TCP port to connect to

                    if (isset($_POST['approve']) && !empty($program_feedback)) {
                        $sql = "UPDATE program_proposal SET program_feedback = ?, "
                                . "program_status = 'A' WHERE proposal_id = ?";
                        if ($decisionQuery = mysqli_prepare($bcmdev, $sql)) {
                            mysqli_stmt_bind_param($decisionQuery, "ss", $program_feedback, $tempID);
                            if (mysqli_stmt_execute($decisionQuery)) {
                                print("<h2>Program Proposal Approved.</h2>");
                            } else {
                                die("Something went wrong. Please try again later.");
                            }
                            mysqli_stmt_close($decisionQuery);

                            $sql = "SELECT ra_username, program_name, program_date FROM program_proposal WHERE proposal_id = '$tempID'";
                            if ($result = $bcmdev->query($sql)) {
                                $row = $result->fetch_assoc();
                                //Recipients
                                $mail->setFrom('bcmdev@uvawise.edu', 'HDoc');
                                $mail->addAddress($row["ra_username"] . '@uvawise.edu');     // Add a recipient
                                $mail->addReplyTo('NoReply@mcs.uvawise.edu', 'DoNotReply');

                                //Content
                                $mail->isHTML(true);                                  // Set email format to HTML
                                $mail->Subject = 'Program Proposal Approved - Do Not Reply';
                                $mail->Body = '<h2>Your program "' . $row["program_name"] . '" ocurring on ' . $row["program_date"] . ' has been approved.</h2>'
                                        . '<br><h3><strong>Feedback:</strong> ' . $program_feedback . '</h3>';

                                $mail->send();
                            } else {
                                die("Program Lookup Query failed.");
                            }
                            echo 'Email has been sent';
                        } else {
                            die("Something went wrong. Please try again later.");
                        }
                    } else if (isset($_POST['reject']) && !empty($program_feedback)) {
                        $sql = "UPDATE program_proposal SET program_feedback = ?, "
                                . "program_status = 'R' WHERE proposal_id = ?";
                        if ($decisionQuery = mysqli_prepare($bcmdev, $sql)) {
                            mysqli_stmt_bind_param($decisionQuery, "ss", $program_feedback, $tempID);
                            if (mysqli_stmt_execute($decisionQuery)) {
                                print("<h2>Program Proposal Rejected.</h2>");
                            } else {
                                die("Something went wrong. Please try again later.");
                            }
                            mysqli_stmt_close($decisionQuery);

                            $sql = "SELECT ra_username, program_name, program_date FROM program_proposal WHERE proposal_id = '$tempID'";
                            if ($result = $bcmdev->query($sql)) {
                                $row = $result->fetch_assoc();
                                //Recipients
                                $mail->setFrom('bcmdev@uvawise.edu', 'HDoc');
                                $mail->addAddress($row["ra_username"] . '@uvawise.edu');     // Add a recipient
                                $mail->addReplyTo('NoReply@mcs.uvawise.edu', 'DoNotReply');

                                //Content
                                $mail->isHTML(true);                                  // Set email format to HTML
                                $mail->Subject = 'Program Proposal Rejected - Do Not Reply';
                                $mail->Body = '<h2>Your program "' . $row["program_name"] . '" ocurring on ' . $row["program_date"] . ' has been rejected.</h2>'
                                        . '<br><h3><b>Feedback:</b> ' . $program_feedback . '</h3>';

                                $mail->send();
                            } else {
                                die("Program Lookup Query failed.");
                            }
                            echo 'Email has been sent';
                        } else {
                            die("Something went wrong. Please try again later.");
                        }
                    } else {
                        ?>
                        <h2>You must enter feedback.</h2>
                        <?php
                    }
                } catch (Exception $e) {
                    echo 'Email could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
            }

            $stmt = "SELECT * FROM program_proposal WHERE program_status = 'P' ORDER BY creation_date DESC";
            if ($result = $bcmdev->query($stmt)) {
                if ($result->num_rows == 0) {
                    ?>
                    <h2>No pending program proposals.</h2>
                    <?php
                } else {
                    ?>
                    <table class="table table-striped">
                        <thead><tr><th>Name</th><th>Username</th><th>Program Name</th><th>Location</th><th>Date</th>
                                <th>Time</th><th>Category</th><th>Description</th><th>Funds</th><th>Items</th>
                                <th>Faculty</th></tr></thead>
                        <tbody>
                            <?php
                            while ($programRow = $result->fetch_assoc()) {
                                ?>
                            <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method='POST'>
                                <tr>
                                    <td class="align-middle"><?php print($programRow["ra_name"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["ra_username"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_name"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_location"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_date"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_time"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_category"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_description"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_funds"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_items"]); ?></td>
                                    <td class="align-middle"><?php print($programRow["program_faculty"]); ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="textarea" class="form-control" placeholder="Enter feedback here..." name="program_feedback">
                                    </td>
                                    <td><input type="submit" class="btn btn-success" name="approve" value="Approve"></td>
                                    <td><input type="submit" class="btn btn-danger" name="reject" value="Reject"></td>
                                    <td><input type='hidden' name='tempId' value='<?php echo $programRow["proposal_id"] ?>'></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </form>
                            <?php
                        }
                    }
                } else {
                    die('Database error. [' . $bcmdev->error . ']');
                }
                $bcmdev->close();
                ?>
            </tbody>
        </table>
    </body>
    </html>
    <?php
} else {
    header("location: index.php");
}

