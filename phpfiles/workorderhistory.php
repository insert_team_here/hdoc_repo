<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header("location: login.php");
    exit;
}
if ($_SESSION['role'] == 'M' || $_SESSION['role'] == 'A' || $_SESSION['role'] == 'P') {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Historical Work Orders</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link href="stylesheet.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                body{ font: 14px sans-serif; background-color: lightblue;}
                .wrappera{ width: 350px; padding: 20px; float: left;}
                .wrapperb{ width: 40%; padding: 20px; float: left;}
                .parentwrapa {margin: auto; width: 700px;}
                .parentwrapb {margin-left: 10%; width: 100%; clear: both;}
            </style>
        </head>
        <body>

            <?php
            include_once 'navbar.php';
            ?>
            <h2>Historical Work Order Search</h2>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <div class="parentwrapa">
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>From:</legend>
                            <input type="date" name="StartDate" required="true" class="form-control"></p>
                        </div>
                    </div>
                    <div class="wrappera">
                        <div class="form-group">
                            <legend>To:</legend>
                            <input type="date" name="EndDate" required="true" class="form-control"></p>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>

                <div class="parentwrapa">
                    <div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form><br>

            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                require_once "/home/bcmdev/include/dbconnect.php";
                #when the button is pressed, get the start and end dates
                $qSDate = trim($_POST['StartDate']);
                $qEDate = trim($_POST['EndDate']);
                #prepare the statement
                $stmt = "SELECT * FROM work_order WHERE date >= '$qSDate' AND date <= '$qEDate'";
                #if result isn't empty, create a table with the data
                if ($result = $bcmdev->query($stmt)) {
                    if ($result->num_rows == 0) {
                        ?>
                        <h2>No results found.</h2>
                        <?php
                    } else {
                        ?>
                        <table class="table table-striped">
                            <thead><tr><th>Date</th><th>Name</th><th>Phone</th><th>Building</th>
                                    <th>Room Number</th><th>Description</th></tr></thead>
                            <tbody>
                                <?php
                                while ($programRow = $result->fetch_assoc()) {
                                    if ($programRow["sub_room"] == "A" || $programRow["sub_room"] == "B" || $programRow["sub_room"] == "C") {
                                        $subroom = $programRow["sub_room"];
                                    } else {
                                        $subroom = "";
                                    }
                                    ?>
                                    <tr>
                                        <td class="align-middle"><?php print($programRow["date"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["ra_name"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["ra_phone"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["bldg_name"]); ?></td>
                                        <td class="align-middle"><?php print($programRow["room_num"] . $subroom); ?></td>
                                        <td class="align-middle"><?php print($programRow["description"]); ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        } else {
                            die('Database error. [' . $bcmdev->error . ']');
                        }
                        $bcmdev->close();
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>
        </body>
    </html>
    <?php
} else {
    header("location: index.php");
}
    