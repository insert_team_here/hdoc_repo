HDoc is a web-based document management system created solely for use by staff of the
Office of Housing and Residence Life at the University of Virginia's College at Wise.

This project was founded by Brian Stacy, Caitlin Emond, and Morgan Davis. This software
is the result of a senior capstone project.
